{#
# Copyright (C) 2021 The Qt Company Ltd.
# Copyright (C) 2019 Luxoft Sweden AB
# SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GPL-3.0-only WITH Qt-GPL-exception-1.0
#}
{% include "common/generated_comment.cpp.tpl" %}
#include <QGuiApplication>
#include <QCommandLineParser>
#include <QDir>
#include <QLockFile>

{% for interface in module.interfaces %}
#include "{{interface|lower}}backend.h"
#include "{{interface|lower}}adapter.h"
{% endfor %}

#include <QtIfRemoteObjectsHelper/QIfRemoteObjectsConfig>
#include <QtInterfaceFramework/QIfSimulationEngine>

{% set defaultMode = "headless" %}
{% set deprecatedCode = false %}
{% if module.tags.config and module.tags.config.defaultServerMode %}
{%   set defaultMode = module.tags.config.defaultServerMode %}
{% elif module.tags.config_simulator and module.tags.config_simulator.defaultServerMode%}
{%   set defaultMode = module.tags.config_simulator.defaultServerMode %}
{%   set deprecatedCode = true %}
Q_LOGGING_CATEGORY(qLcRO, "{{module|qml_type|lower}}.server.remoteobjects", QtInfoMsg)
{% endif %}
{% set validModes = ["headless", "gui"] %}
{% if targetPlatform == "Android" %}
{%   set validModes = validModes + ['android'] %}
#include <QtCore/private/qandroidextras_p.h>
{% endif %}
{% if defaultMode not in validModes %}
{{   error("Unknown value in 'defaultServerMode'. Valid modes are: " ~ validModes|join(', ') ~ ".") }}
{% endif %}
using namespace Qt::StringLiterals;
{% set ns = module|namespace %}
{% if ns|length %}
using namespace {{ns}};
{% endif %}

int main(int argc, char *argv[])
{
{% if deprecatedCode %}
    qCInfo(qLcRO) << "Using config_simulator.defaultServerMode is deprecated and will be removed "
                     "in future Qt versions.";
    qCInfo(qLcRO) << "Please use the new config.defaultServerMode annotation to configure default server mode.";
{% endif %}

    const QStringList validModes = { {% for mode in validModes -%}"{{ mode }}"{% if not loop.last %}, {% endif %}{% endfor %} };
    QString appType = "{{ defaultMode }}";
    // Loop through the command-line arguments
    for (int i = 1; i < argc; i++) {
        if (qstrncmp(argv[i], "--", 2) == 0) {
            QString mode = QString(argv[i]).mid(2);
            if (validModes.contains(mode)) {
                appType = mode;
                break;
            }
         }
     }

    QScopedPointer<QCoreApplication> app;
    if (appType == "gui")
        app.reset(new QGuiApplication(argc, argv));
{% if targetPlatform == "Android" %}
    else if(appType =="android")
        app.reset(new QAndroidService(argc, argv));
{% endif %}
    else
        app.reset(new QCoreApplication(argc, argv));

    QCommandLineParser parser;
    parser.addHelpOption();

    QCommandLineOption guiOption(u"gui"_s, u"Gui mode. Starts using a QGuiApplication and allows "
                                           "instantiating visual elements in the simulation code"_s);
    parser.addOption(guiOption);
    QCommandLineOption headlessOption(u"headless"_s, u"Headless mode. Starts using a QCoreApplication "
                                                      "and does NOT allow instantiating visual elements "
                                                      "in the simulation code"_s);
    parser.addOption(headlessOption);
{% if targetPlatform == "Android" %}
    QCommandLineOption androidOption(u"android"_s, u"Android mode. Starts using a QAndroidService "
                                                      "in the simulation code"_s);
    parser.addOption(androidOption);
{% endif %}

    QCommandLineOption serverUrlOption(u"serverUrl"_s, u"The serverUrl to use for all Remote Objects hosted in this server"_s, u"url"_s);
    parser.addOption(serverUrlOption);

    QCommandLineOption confOption(u"serverConf"_s, u"A config file which host url for all Remote Objects"_s, u"file"_s);
    parser.addOption(confOption);

    parser.process(qApp->arguments());

    // single instance guard
    QLockFile lockFile(u"%1/%2.lock"_s.arg(QDir::tempPath(), qApp->applicationName()));
    if (!lockFile.tryLock(100)) {
        qCritical("%s already running, aborting...", qPrintable(qApp->applicationName()));
        return EXIT_FAILURE;
    }

    QIfRemoteObjectsConfig config;
    if (parser.isSet(serverUrlOption))
        config.setDefaultServerUrl(parser.value(serverUrlOption));
    if (parser.isSet(confOption))
        config.parseConfigFile(parser.value(confOption));
    if (!parser.isSet(serverUrlOption) && !parser.isSet(confOption))
        config.parseLegacyConfigFile();

    auto simulationEngine = new QIfSimulationEngine(u"{{module.name|lower}}"_s);

{% for interface in module.interfaces %}
    auto {{interface|lowerfirst}}Instance = new {{interface}}Backend(simulationEngine);
    //Register the types for the SimulationEngine
    {{module.module_name|upperfirst}}::registerQmlTypes(u"{{module|qml_type}}.simulation"_s, {{module.majorVersion}}, {{module.minorVersion}});
    simulationEngine->registerSimulationInstance({{interface|lowerfirst}}Instance, "{{module|qml_type}}.simulation", {{module.majorVersion}}, {{module.minorVersion}}, "{{interface}}Backend");
{% endfor %}
{% if module.tags.config_simulator and module.tags.config_simulator.simulationFile %}
{%   set simulationFile = module.tags.config_simulator.simulationFile %}
{% else %}
{%   set simulationFile = "qrc:///simulation/" + module.module_name|lower + '_simulation.qml' %}
{% endif %}
    simulationEngine->loadSimulationData(u":/simulation/{{module.module_name|lower}}_simulation_data.json"_s);
    simulationEngine->loadSimulation(QUrl(u"{{simulationFile}}"_s));

    //initialize all our backends
{% for interface in module.interfaces %}
    {{interface|lowerfirst}}Instance->initialize();
{%   for property in interface.properties %}
{%     if property.type.is_model %}
    {{interface|lowerfirst}}Instance->{{property|getter_name}}()->initialize();
{%     endif %}
{%   endfor %}
{% endfor %}

    //Start Remoting the backends
{% for interface in module.interfaces %}
    auto {{interface|lowerfirst}}Adapter = new {{interface}}QtRoAdapter({{interface|lowerfirst}}Instance);
    {{interface|lowerfirst}}Adapter->enableRemoting(config.host(u"{{module}}"_s, u"{{interface}}"_s));
{% endfor %}

    return qApp->exec();
}
