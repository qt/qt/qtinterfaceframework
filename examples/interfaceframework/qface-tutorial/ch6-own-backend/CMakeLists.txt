cmake_minimum_required(VERSION 3.16)
project(qface-chapter6 LANGUAGES CXX)

find_package(Qt6 REQUIRED COMPONENTS Core InterfaceFramework Qml Quick)

qt_standard_project_setup(REQUIRES 6.7)

if(NOT DEFINED INSTALL_EXAMPLESDIR)
    set(INSTALL_EXAMPLESDIR "examples")
endif()

set(INSTALL_EXAMPLEDIR "${INSTALL_EXAMPLESDIR}/interfaceframework/qface-tutorial/chapter6-own-backend")

add_subdirectory(instrument-cluster)
add_subdirectory(frontend)
add_subdirectory(backend_simulator)
add_subdirectory(backend_dbus)
add_subdirectory(demo_server)
